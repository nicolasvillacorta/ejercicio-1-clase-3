package com.captton.programa;
import com.captton.clases.*;

public class Inicio {

	public static void main(String[] args) {
		Jugador Zarate = new Jugador("Mauro Zarate", 31, 25734);
		Jugador Kun = new Jugador("Sergio Aguero", 29, 800000);
		Jugador Pato = new Jugador("Roberto Abbondanzieri", 40, 60000);
		Club Boquita = new Club("Boca Juniors");
		Club RealM = new Club("Real Madrid");

		Auto renault = new Auto("Renault 12", 20000, "EHT 404");
		Auto fitito = new Auto("El fitito", 999, "AAA 100");
		
		Zarate.leSacaronRoja();
		System.out.println(Zarate.getSalario());
		Zarate.comprarAuto(renault);
		Zarate.comprarAuto(fitito);
		Zarate.mostrarAutos();
		System.out.println("---------------------------");
		RealM.comprarJugador(Pato);
		RealM.comprarJugador(Kun);
		Boquita.comprarJugador(Zarate);
		Zarate.leSacaronAmarilla();
		Boquita.mostrarJugadores();
		System.out.println("---------------------------");
		RealM.mostrarJugadores();
		System.out.println("---------------------------");
		Pato.leSacaronAmarilla();
		Pato.leSacaronAmarilla();
		Kun.leSacaronAmarilla();
		
		RealM.amarillasDelPlantel();
		System.out.println(Zarate.getClub().getNombre());
		RealM.comprarJugador(Zarate);
		RealM.mostrarJugadores();
		Boquita.mostrarJugadores();
		Boquita.amarillasDelPlantel();
		Zarate.leSacaronAmarilla();
		RealM.amarillasDelPlantel();
		
	}

}
