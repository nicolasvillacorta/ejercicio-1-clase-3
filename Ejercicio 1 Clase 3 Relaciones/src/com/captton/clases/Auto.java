package com.captton.clases;

public class Auto {
	private String nombre;
	private int valor;
	private String patente;
	public String getNombre() {
		return nombre;
	}
	
	
	
	public Auto(String nombre, int valor, String patente) {
		super();
		this.nombre = nombre;
		this.valor = valor;
		this.patente = patente;
	}



	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getValor() {
		return valor;
	}
	public void setValor(int valor) {
		this.valor = valor;
	}
	public String getPatente() {
		return patente;
	}
	public void setPatente(String patente) {
		this.patente = patente;
	}

	
	
}
