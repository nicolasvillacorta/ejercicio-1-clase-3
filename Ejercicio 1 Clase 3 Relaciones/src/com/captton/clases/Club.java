package com.captton.clases;

import java.util.ArrayList;

public class Club {
	private String nombre;
	private ArrayList<Jugador> jugadores;

	
	public void comprarJugador(Jugador Jugador) {
		this.jugadores.add(Jugador);
		if(Jugador.getClub()!=null)
		{
			int aux = Jugador.getClub().getJugadores().indexOf(Jugador);
			Jugador.getClub().getJugadores().remove(aux);
		}
		
		Jugador.setClub(this);
	}
	
	public void mostrarJugadores() {
		System.out.println("El equipo "+this.getNombre()+" posee los siguientes jugadores: ");
		for(Jugador pl: this.jugadores) {
			System.out.println(pl.getNombreCompleto());
		}
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public Club(String nombre) {
		this.nombre = nombre;
		this.jugadores = new ArrayList<Jugador>(); 
		
	}
	
	public void amarillasDelPlantel() {
		int cont = 0;
		for(Jugador ju: this.jugadores) {
			cont = cont+ju.getAmarillas();
		}
		System.out.println("El "+this.nombre+ " tiene un total de "+cont+" tarjetas amarillas.");
		
	}

	public ArrayList<Jugador> getJugadores() {
		return jugadores;
	}

	public void setJugadores(ArrayList<Jugador> jugadores) {
		this.jugadores = jugadores;
	}
	
	
}
