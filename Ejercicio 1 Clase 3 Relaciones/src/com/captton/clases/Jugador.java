package com.captton.clases;
import java.util.ArrayList;

public class Jugador {
	private String nombreCompleto;
	private int edad;
	private Club club;
	private float salario;
	private ArrayList<Auto> autos;
	private int amarillas;
	private int rojas;
	
	public Jugador(String nombre, int edad, float salario) {
		this.nombreCompleto = nombre;
		this.edad = edad;
		this.salario = salario;
		this.autos = new ArrayList<Auto>();
		
	}
	
	public int getRojas() {
		return rojas;
	}
	
	public int getAmarillas() {
		return amarillas;
	}
	
	public void mostrarAutos() {
		System.out.println("Autos de "+this.nombreCompleto);
		for(Auto au: this.autos) {
			System.out.println("Auto: "+au.getNombre()+" | Patente: "+au.getPatente());
		}
	}
	
	public void comprarAuto(Auto auto) {
		this.autos.add(auto);
	}
	
	
	public void meterGol() {
		this.salario = salario+((5*this.salario)/100);
	}
	
	public void leSacaronAmarilla() {
		this.amarillas++;
	}
	
	public void leSacaronRoja() {
		this.salario = salario-((5*this.salario)/100);
		this.rojas++;
	}

	public String getNombreCompleto() {
		return nombreCompleto;
	}

	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public Club getClub() {
		return club;
	}

	public void setClub(Club club) {
		this.club = club;
	}

	public float getSalario() {
		return salario;
	}

	public void setSalario(int salario) {
		this.salario = salario;
	}
	
	
}
